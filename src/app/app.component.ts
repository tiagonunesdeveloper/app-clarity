import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(private apiServer: AppService) { }

  ngOnInit(): void {
    this.apiServer.serverApiStatus().subscribe(res => console.log(`PING : ${res.Data.response}`));
  }
}
