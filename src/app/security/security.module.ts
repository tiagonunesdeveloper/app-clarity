import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { LoginComponent } from './login/login.component';
import { SecurityRoutingModule } from './security.routes';
import { SecurityService } from './security.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    SecurityRoutingModule,
  ],
  providers: [SecurityService],
  declarations: [LoginComponent]
})
export class SecurityModule { }
