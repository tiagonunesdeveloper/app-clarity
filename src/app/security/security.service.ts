import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { User } from '../models/user';


@Injectable()
export class SecurityService {
  private BASE_URL = 'http://localhost:12345/api/';

  constructor(private http: HttpClient) { }

  public serverApiStatus(): Observable<any> {
    const url = `${this.BASE_URL}/ping`;
    return this.http.get(url);
  }

  public getToken(): string {
    return localStorage.getItem('token');
  }

  public logIn(email: string, password: string): Observable<any> {
    const url = `${this.BASE_URL}/login`;
    return this.http.post<User>(url, { email, password });
  }

  public signUp(email: string, password: string): Observable<User> {
    const url = `${this.BASE_URL}/register`;
    return this.http.post<User>(url, { email, password });
  }
}
