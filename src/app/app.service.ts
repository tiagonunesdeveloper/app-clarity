import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class AppService {
  private BASE_URL = 'http://localhost:12345/api/';

  constructor(private http: HttpClient) { }

  public serverApiStatus(): Observable<any> {
    const url = `${this.BASE_URL}/ping`;
    return this.http.get(url);
  }

}
